<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Heroe;
use App̈́\Height;

class HeroeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try {
            $param = [];
            
            foreach(request()->all() as $key => $value) {
                switch ($key) {
                    case 'id':
                        $param[$key] = $value;
                        break;
                    case 'name':
                        $param[$key] = $value;
                        break;
                    case 'fullName':
                        $param[$key] = $value;
                        break;
                    case 'strength':
                        $param[$key] = $value;
                        break;
                    case 'speed':
                        $param[$key] = $value;
                        break;
                    case 'durability':
                        $param[$key] = $value;
                        break;
                    case 'power':
                        $param[$key] = $value;
                        break;
                    case 'combat':
                        $param[$key] = $value;
                        break;
                    case 'race':
                        $param[$key] = $value;
                        break;
                    case 'eyeColor':
                        $param[$key] = $value;
                        break;
                    case 'hairColor':
                        $param[$key] = $value;
                        break;
                    case 'publisher':
                        $param[$key] = $value;
                        break;
                }
            }
            
            //$heroes = Heroe::with(['heights','weights'])->where($param)->get();
	    $heroes = Heroe::with(['heights','weights'])->where($param)
            ->orderBy('name','desc')
            ->paginate();

            return response()->json(['status' => true, 'message' => '', 'heroes' => $heroes], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage(), 'heroes' => []], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
