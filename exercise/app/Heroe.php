<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heroe extends Model
{
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','fullName','strength','speed','durability','power','combat','race','eyeColor','hairColor','publisher',
    ];

    public function heights(){
        return $this->hasMany('App\Height');
    }

    public function weights(){
        return $this->hasMany('App\Weight');
    }
}
