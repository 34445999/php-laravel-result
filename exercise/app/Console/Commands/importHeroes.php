<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Heroe;
use App\Weight;
use App\Height;
use File;

class importHeroes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:heroes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try
        {
            //$contents = File::get('/var/www/html/php-laravel-exercise/csv/superheros.csv');
            $contents = fopen('/var/www/html/php-laravel-exercise/csv/superheros.csv','r');
            $i = 0;
            while (($line = fgets($contents)) !== false) {
                if($i != 0){
                    $array_heroe = explode(',', $line);
                    
                    $heroe = Heroe::create([
                        'id'=>$array_heroe[0],
                        'name'=>$array_heroe[1],
                        'fullName'=>$array_heroe[2],
                        'strength'=>$array_heroe[3],
                        'speed'=>$array_heroe[4],
                        'durability'=>$array_heroe[5],
                        'power'=>$array_heroe[6],
                        'combat'=>$array_heroe[7],
                        'race'=>$array_heroe[8],
                        'eyeColor'=>$array_heroe[13],
                        'hairColor'=>$array_heroe[14],
                        'publisher'=>$array_heroe[15]
                    ]);

                    if($array_heroe[9] != ''){
                        $obj_height = new Height();
                        $obj_height->height = $array_heroe[9];
                        $obj_height->heroe()->associate($heroe);
                        $obj_height->save();
                    }

                    if($array_heroe[10] != ''){
                        $obj_height = new Height();
                        $obj_height->height = $array_heroe[10];
                        $obj_height->heroe()->associate($heroe);
                        $obj_height->save();
                    }

                    if($array_heroe[11] != ''){
                        $obj_weight = new Weight();
                        $obj_weight->weight = $array_heroe[11];
                        $obj_weight->heroe()->associate($heroe);
                        $obj_weight->save();
                    }

                    if($array_heroe[12] != ''){
                        $obj_weight = new Weight();
                        $obj_weight->weight = $array_heroe[12];
                        $obj_weight->heroe()->associate($heroe);
                        $obj_weight->save();
                    }
                }
                $i +=1;
            }

            echo "Los datos han sido importados";
            
        }
        catch(\Exception $e){
            die($e->getMessage());
        }
    }
}
