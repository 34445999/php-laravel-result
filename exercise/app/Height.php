<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Height extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'height',
    ];

    public function heroe(){
        return $this->belongsTo('App\Heroe');
    }
}
