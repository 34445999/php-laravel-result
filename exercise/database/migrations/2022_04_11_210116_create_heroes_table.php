<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->primary('id');
            $table->string('name')->nullable(); 
            $table->string('fullName')->nullable(); 
            $table->string('strength')->nullable(); 
            $table->string('speed')->nullable(); 
            $table->string('durability')->nullable(); 
            $table->string('power')->nullable(); 
            $table->string('combat')->nullable(); 
            $table->string('race')->nullable(); 
            $table->string('eyeColor')->nullable(); 
            $table->string('hairColor')->nullable(); 
            $table->string('publisher')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heroes');
    }
}
