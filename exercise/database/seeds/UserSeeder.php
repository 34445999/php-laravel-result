<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        'name'=>'test',
        'email'=>'test@example.com',
        'password'=>Hash::make('123'),
        ]);
    }
}
