GUÍA DE INSTALACIÓN:

Clonar el repositorio: git clone git@bitbucket.org:34445999/php-laravel-result.git 
Una vez que haya clonado el repositorio, deberá crear una base de datos MySQL
Configurar conexión a su base de datos en el archivo .env

Mantener la ubicación actual del archivo.cvs para que script pueda cargar la información en la base de datos.
Abrir la consola, posicionarse en la raiz del proyecto y ejecutar los siguientes comandos:
composer install
php artisan migrate:fres --seed
php artisan import:heroes

Iniciar el servidor con el comando:
php artisan serve (peticiones http de forma local)
php artisan serve --host 'ip' --port 'puerto' (peticiones http de forma remota)

El endpoint requiere la utilización del token de autenticación que deberá ser cargado como "Bearer Token" en cada petición. 
Para obtener un token posicionarse en la raiz del proyecto y ejecutar los siguientes comandos:
php artisan tinker
$user = User::find(1)
$token = $user->createToken('token-name');
$token->plainTextToken;


El endpoint para peticiones de forma local es:
Url: http://127.0.0.1:8000/api/heroes/show
Metodo: GET
Authorization (Bearer Token)
Parametro permitidos: 'id','name','fullName','strength','speed','durability','power','combat','race','eyeColor',
'hairColor','publisher'

Ejemplo: http://127.0.0.1:8000/api/heroes/show?name=Abomination&fullname=Emil Blonsky



